#!.venv/bin/python

from subprocess import Popen
from config import basedir
import shlex


def setup():
    path = basedir + "/.venv/bin/python "
    cmds = ["db_crate.py", "db_migrate.py", "run.py"]
    for raw_cmd in cmds:
        print path + raw_cmd
        cmd = shlex.split(path + raw_cmd)
        proc = Popen(cmd)
        proc.wait()


if __name__ == "__main__":
    setup()
