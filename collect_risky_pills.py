#!.venv/bin/python

from task.remote_api_handlers import AllPillsHandler
import logging
from config import basedir

logging.getLogger("urllib3").setLevel(logging.CRITICAL)


def setup_logger():
    fmt = u'%(filename)s:%(levelname)-8s [%(asctime)s]  %(message)s'
    logging.basicConfig(
        filename=basedir + '/' + 'pills.log',
        level=logging.DEBUG,
        format=fmt,
        datefmt='%m/%d/%Y %H:%M:%S'
    )

if __name__ == "__main__":
    setup_logger()
    logging.info("Hope my laptop won't get burned. STARTING")
    h = AllPillsHandler()
    h.run()
    logging.info("It is morning already!!! FINISH")
    logging.info("Ahh.. NO it was a small combinations range.")


