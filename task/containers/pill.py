#!.venv/bin/python

from __future__ import unicode_literals
import requests


class Pill(object):

    def __init__(self, classId, className, classType):
        self.classId = classId
        self.className = className
        self.classType = classType
        self.pillCode = []

    def get_pill_once_code(self, url):
        key = "rxnormId"
        r = requests.get(url)
        res = r.json()["idGroup"]
        if key in res.keys():
            self.pillCode = [c for c in res[key]]
            return self
        return None

    def get_name(self):
        return self.className

    def get_code(self):
        return self.pillCode
