#!.venv/bin/python

from __future__ import unicode_literals
import requests
import json


class RiskyPillsCombinations(object):

    def __init__(self):
        self.needle = "The risk or severity of adverse effects can be increased when".split(" ")
        self.combinations = []

    def get_combination_once_risky(self, url, pills):
        r = requests.get(url)
        if all(s in r.text for s in self.needle):
            return pills
        return None

    def get_combinations(self):
        return self.combinations

    def append_combination(self, c):
        self.combinations.append(c)

    def flush_combinations(self):
        del self.combinations[:]
