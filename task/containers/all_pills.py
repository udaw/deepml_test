#!.venv/bin/python

from task.containers import Pill


class AllPills(object):

    def __init__(self):
        self.pills_with_codes = []

    def set_pills_with_codes(self, pills):
        self.pills_with_codes = pills

    def get_pills_with_codes(self):
        return self.pills_with_codes

    def gen_pills(self, *pills):
        for pill in pills:
            yield Pill(
                classId=pill["classId"],
                className=pill["className"],
                classType=pill["classType"]
            )
