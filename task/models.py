from task import db


class RiskyPillCombination(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstClassId = db.Column(db.String(244), index=True)
    firstClassName = db.Column(db.String(244), index=True)
    firstClassType = db.Column(db.String(244), index=True)
    firstPillCode = db.Column(db.String(244), index=True)
    secondClassId = db.Column(db.String(244), index=True)
    secondClassName = db.Column(db.String(244), index=True)
    secondClassType = db.Column(db.String(244), index=True)
    secondPillCode = db.Column(db.String(244), index=True)

    def __repr__(self):
        return '<Pill %r>' % (self.firstClassId)
