

class ResourceUnavailableException(ValueError):

    def __init__(self, *args, **kwargs):
        super(ResourceUnavailableException, self).__init__(*args, **kwargs)
