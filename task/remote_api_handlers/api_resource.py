#!.venv/bin/python

from task.task_exceptions import ResourceUnavailableException
from urllib import quote


class ApiResource(object):

    def __init__(self):
        self.public_base = "https://rxnav.nlm.nih.gov/REST/rxclass/"
        self.base = "https://rxnav.nlm.nih.gov/REST/"
        self.formats = {
            "json": ".json",
            "xml": ".xml"
        }
        self.resources = [
            "interaction/list",
            "rxcui",

            "allClasses",
            "class/byId",
            "class/byName",
            "class/byDrugName",
            "class/byRxcui",
            "classContext",
            "classGraph",
            "classMembers",
            "classTree",
            "classTypes",
            "relas",
            "relaSources",
            "similar",
            "similarByRxcuis",
            "similarInfo",
            "spellingsuggestions",
        ]

    def _get_resource(self, needle):
        try:
            return self.resources[self.resources.index(needle)]
        except ValueError as e:
            raise ResourceUnavailableException("Resource unavailable")

    def _allClasses(self, resource, format="json", **kwargs):
        return self.public_base + resource + self.formats[format]

    def _class_byId(self, resource, format="json", **kwargs):
        pass

    def _interaction_list(self, resource, format="json", **kwargs):
        req = "?rxcuis="
        for pill in kwargs["pills"]:
            for code in pill.get_code():
                req += code + "+"
        req = req.rstrip("+")
        return self.base + resource + self.formats[format] + req

    def _rxcui(self, resource, format="json", **kwargs):
        req = "?"
        for key, value in kwargs.iteritems():
            req += key + "=" + quote(value) + "&"
        req = req.rstrip("&")
        return self.base + resource + self.formats[format] + req

    def get_resource(self, needle, **kwargs):
        resource = self._get_resource(needle)
        name = "_" + needle.replace("/", "_")
        method = None

        try:
            method = getattr(self, name)
            return method(resource, **kwargs)
        except AttributeError as e:
            raise ResourceUnavailableException("Resource is currently unavailable.")
