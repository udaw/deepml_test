#!.venv/bin/python

from __future__ import unicode_literals
from task.containers import AllPills, RiskyPillsCombinations
from task.remote_api_handlers import ApiResource
import requests
from concurrent.futures import ThreadPoolExecutor, as_completed
from time import time
from datetime import datetime
from task import db, models
from itertools import combinations
import logging


COMB_CHUNK_LIMIT = 20000


class AllPillsHandler(object):

    def __init__(self):
        self.resource = ApiResource()
        self.allPills = AllPills()
        self.riskyPills = RiskyPillsCombinations()

    def _persist(self):
        for c in self.riskyPills.get_combinations():
            db_pill = models.RiskyPillCombination(
                firstClassId=c[0].classId,
                firstClassName=c[0].className,
                firstClassType=c[0].classType,
                firstPillCode=", ".join(c[0].pillCode),
                secondClassId=c[1].classId,
                secondClassName=c[1].className,
                secondClassType=c[1].classType,
                secondPillCode=", ".join(c[1].pillCode)
            )
            db.session.add(db_pill)
        db.session.commit()
        self.riskyPills.flush_combinations()

    def _get_pills(self):
        url = self.resource.get_resource("allClasses")
        r = requests.get(url)
        return r.json()["rxclassMinConceptList"]["rxclassMinConcept"]

    def _set_pills_with_codes(self):
        all_pills = self._get_pills()
        logging.info("Overall PILLS: {}".format(len(all_pills)))
        pool = ThreadPoolExecutor()
        futures = (
            pool.submit(
                p.get_pill_once_code,
                self.resource.get_resource("rxcui", **{"name": p.get_name()}))
            for p in self.allPills.gen_pills(*all_pills)
            )
        self.allPills.set_pills_with_codes([r.result() for r in as_completed(futures) if r.result() is not None])

    def _fetch_combinations(self, source):
        t = datetime.now()
        logging.info("Going to work a little bit with chunk: {} len".format(len(source)))

        pool = ThreadPoolExecutor()
        futures = (
            pool.submit(
                self.riskyPills.get_combination_once_risky,
                self.resource.get_resource("interaction/list", **{"pills": c}),
                c
                )
            for c in source
            )
        [self.riskyPills.append_combination(r.result()) for r in as_completed(futures) if r.result() is not None]
        print "Step Done."
        lap_t = datetime.now() - t
        logging.info("Piece of cake! Only took {} to handle this chunk.".format(lap_t))
        self._persist()
        lap_saved = datetime.now() - t
        logging.info("Plus some little time to store all stuff {} to handle this lap.".format(lap_saved - lap_t))

    # def _gen_risky_combinations(self):
    #     q = 0
    #     pool = ThreadPoolExecutor()
    #     futures = (
    #         pool.submit(
    #             self.riskyPills.get_combination_once_risky,
    #             self.resource.get_resource("interaction/list", **{"pills": c}),
    #             c
    #             )
    #         for c in combinations(self.allPills.get_pills_with_codes(), 2)
    #         )
    #     for r in as_completed(futures):
    #         q += 1
    #         print r.result()
    #     print "Combinations Quantity:"
    #     print q

    def _gen_risky_combinations_chunk(self):
        risky_iter = combinations(self.allPills.get_pills_with_codes(), 2)
        pills_buffer = []
        limit = 0
        try:
            while limit <= COMB_CHUNK_LIMIT:
                pills_buffer.append(risky_iter.next())
                limit += 1
                if limit == COMB_CHUNK_LIMIT:
                    self._fetch_combinations(pills_buffer)
                    pills_buffer = []
                    limit = 0
        except StopIteration as e:
            logging.warning("Victory!!! Looks like we almost did it.\nOnly left to store some final items.")
            logging.exception(e)
            self._fetch_combinations(pills_buffer)
            return

    def run(self):
        then = time()
        self._set_pills_with_codes()
        self._gen_risky_combinations_chunk()
        # print self.riskyPills.combinations
        print "Done in %s" % (time() - then)
        logging.info("If we'll ever reach this point.\nThan we're done in %s" % (time() - then))
