from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api


app = Flask(__name__, static_url_path="")
api = Api(app)
app.config.from_object('config')
db = SQLAlchemy(app)

from task_api.pills import TaskListAPI
api.add_resource(TaskListAPI, '/tasks', endpoint='tasks')

from task import views, models
