#!.venv/bin/python

from flask_restful import Resource, reqparse, fields, marshal
from task import db, models


task_fields = {
    "firstClassId": fields.String,
    "firstClassName": fields.String,
    "firstClassType": fields.String,
    "firstPillCode": fields.String,
    "secondClassId": fields.String,
    "secondClassName": fields.String,
    "secondClassType": fields.String,
    "secondPillCode": fields.String,
    'uri': fields.Url('tasks')
}


class TaskListAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('title', type=str, required=True,
                                   help='No task title provided',
                                   location='json')
        self.reqparse.add_argument('description', type=str, default="",
                                   location='json')
        super(TaskListAPI, self).__init__()

    def get(self):
        pills = models.RiskyPillCombination.query.all()
        return {'tasks': [marshal(pill, task_fields) for pill in pills]}
